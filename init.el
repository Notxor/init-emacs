;;; package --- init.el
;;; Commentary:
;;; Este es mi fichero init.el para configurar Emacs
(require 'package)
;;; Code:
(setq package-archives
      '(("elpa"   . "https://elpa.gnu.org/packages/")
        ;;("devel"  . "https://elpa.gnu.org/devel/")
        ("nongnu" . "https://elpa.nongnu.org/nongnu/")
        ("melpa"  . "https://melpa.org/packages/")))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-and-compile
  (setq use-package-always-ensure t
        use-package-expand-minimally t))

;; lugar para los paquetes bajados a mano
(add-to-list 'load-path "~/.emacs.d/site-packages")
(add-to-list 'load-path "~/.emacs.d/notxor-blog/")

;; paquete delight para cambiar el nombre de los modos al instalarlos
(use-package delight
  :ensure t)

(use-package dracula-theme
  :ensure t)

(require 'ox-latex)

(setq custom-file "~/.emacs.d/mi-custom-var.el")
(load custom-file)

(use-package flyspell
  :custom
  (ispell-program-name "aspell")
  (ispell-default-dictionary "espanol")
  (ispell-list-command "--list")
  :hook (text-mode . flyspell-mode)
  :bind (("M-<f7>" . flyspell-buffer)))

(use-package flyspell-correct
  :after (flyspell)
  :bind (("C-;" . flyspell-auto-correct-previous-word)
         ("<f7>" . flyspell-correct-wrapper)))

;; Asignación de combinaciones de teclas globales
(global-set-key (kbd "C-v")     'iedit-mode)
(global-set-key (kbd "C-c a")   'org-agenda)
(global-set-key (kbd "C-c c")   'org-capture)
(global-set-key (kbd "C-c d")   'org-date-from-calendar)
(global-set-key (kbd "C-c e")   'elfeed)
(global-set-key (kbd "C-c g")   'counsel-grep)
(global-set-key (kbd "C-x g")   'magit-status)
(global-set-key (kbd "C-c i")   'counsel-imenu)
(global-set-key (kbd "C-c j")   'join-line)
(global-set-key (kbd "C-c h")   'hl-line-mode)
(global-set-key (kbd "C-c l")   'display-line-numbers-mode)
(global-set-key (kbd "C-c m")   'which-key-show-major-mode)
(global-set-key (kbd "C-c r")   'swiper)
(global-set-key (kbd "C-c t")   'toggle-truncate-lines)
(global-set-key (kbd "C-c w")   'whitespace-mode)
(global-set-key (kbd "<C-return>") 'hs-toggle-hiding)
(global-set-key (kbd "M-/")     'hippie-expand)
;; Configuración de auto-complete
(global-set-key (kbd "M-TAB") 'auto-complete)

;; Hooks asignados automáticamente a modos
(add-hook 'text-mode-hook 'turn-on-auto-fill)
(add-hook 'text-mode-hook 'turn-on-flyspell)
(add-hook 'text-mode-hook 'hl-line-mode)
(add-hook 'dired-mode-hook 'auto-revert-mode) ; refresco de dired
(add-hook 'ledger-mode-hook 'display-line-numbers-mode)
(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(add-hook 'prog-mode-hook 'hs-minor-mode) ; para poder plegar funciones
(add-hook 'prog-mode-hook 'hl-line-mode)
(add-hook 'prog-mode-hook 'flymake-mode)
(add-hook 'prog-mode-hook 'electric-pair-mode) ; activar pareja de paréntesis, llaves, etc.
(add-hook 'gdscript-mode 'whitespace-mode)
(add-hook 'org-mode-hook 'org-superstar-mode) ; activar modo de visualización de cabeceras de un punto
(add-hook 'ediff-prepare-buffer-hook #'show-all)
(add-hook 'xref-backend-functions #'dumb-jump-xref-activate) ; activar «jump to definition»

(use-package org-appear
  :ensure t
  :defer t
  :hook
  (org-mode . org-appear-mode))

(use-package org-superstar
  :ensure t
  :defer t)

(use-package editorconfig
  :ensure t
  :config
  (editorconfig-mode t))

;; Resalte de la posición del cursor al cambiar de `buffer'
(use-package beacon
  :ensure t
  :config
  (beacon-mode t))

;; Mejorando la visualización de la línea de estado
(use-package powerline
  :ensure t
  :config
  (powerline-default-theme)
  (setq powerline-default-separator 'rounded))

(use-package demap
  :defer t)

(global-set-key (kbd "<f4>") 'demap-toggle)

(use-package heaven-and-hell
  :ensure t
  :init
  (setq heaven-and-hell-theme-type 'dark) ;; Omit to use light by default
  (setq heaven-and-hell-themes
        '((light . tango)
          (dark . dracula))) ;; Se puede sustituir por (dark . (tsdh-dark wombat))
  ;; Optionall, load themes without asking for confirmation.
  (setq heaven-and-hell-load-theme-no-confirm t)
  :hook ((after-init . heaven-and-hell-init-hook)))

(defun ajuste-powerline-theme ()
  "Hace un reset de la powerline tras cambiar el tema."
  (interactive)
  (heaven-and-hell-toggle-theme)
  (powerline-reset))

(defun ajuste-powerline-default-theme ()
  "Hace un reset de la powerline tras cambiar al tema por defecto."
  (interactive)
  (heaven-and-hell-load-default-theme)
  (powerline-reset))

(global-set-key (kbd "C-c <f6>") 'ajuste-powerline-default-theme)
(global-set-key (kbd "<f6>")     'ajuste-powerline-theme)

;; Fondo de Emacs transparente
;;(set-frame-parameter (selected-frame) 'alpha '(92 . 90))
;;(add-to-list 'default-frame-alist '(alpha . (92 . 90)))

;; Activar plegado de código con hs-minor-mode
;; HIDE
(global-set-key (kbd "M-p t") 'hs-hide-all)
(global-set-key (kbd "M-p d") 'hs-hide-block)
(global-set-key (kbd "M-p c") 'hs-hide-comment-region)
(global-set-key (kbd "M-p l") 'hs-hide-level)
;; SHOW
(global-set-key (kbd "M-p a") 'hs-show-all)
(global-set-key (kbd "M-p s") 'hs-show-block)
;; TOGGLE
(global-set-key (kbd "M-p e") 'hs-toggle-hiding)
;; JUMP
(global-set-key (kbd "M-p i") 'imenu)

(use-package nerd-icons
  :ensure t)

(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))

(setq dashboard-startupify-list '(dashboard-insert-banner
                                  dashboard-insert-newline
                                  dashboard-insert-banner-title
                                  dashboard-insert-newline
                                  dashboard-insert-navigator
                                  dashboard-insert-newline
                                  dashboard-insert-init-info
                                  dashboard-insert-items
                                  dashboard-insert-newline
                                  dashboard-insert-footer))
(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
;; Iconos
(setq dashboard-icon-type 'nerd-icons)
(setq dashboard-display-icons-p t)
(setq dashboard-set-heading-icons t)
(setq dashboard-set-file-icons t)
(dashboard-modify-heading-icons '((recents   . "nf-oct-file")
                                  (projects  . "nf-oct-rocket")
                                  (bookmarks . "nf-oct-bookmark")
                                  (agenda    . "nf-oct-calendar")
                                  (registers . "nf-oct-note")))

;; Establece el título
(setq dashboard-banner-logo-title "Bienvenido a Emacs Dashboard")
;; Títulos
(setq dashboard-item-names '(("Recent Files:"               . "Archivos recientes:")
                             ("Projects:"                   . "Proyectos:")
                             ("Registers:"                  . "Registros:")
                             ("Agenda for the coming week:" . "Agenda para la semana:")))
;; Establece la cartelera
(setq dashboard-startup-banner 'logo)
(setq dashboard-items '((recents  . 5)
                        (bookmarks . 5)
                        (projects . 5)
                        (agenda . 10)
                        (registers . 5)))

;; Centra el contenido de la pantalla
(setq dashboard-center-content t)

;; Mostar el atajo de salto al lado de la cabecera
(setq dashboard-show-shortcuts t)

(use-package counsel
  :ensure t)

;; Configurar ivy
;; add ‘recentf-mode’ and bookmarks to ‘ivy-switch-buffer’.
(use-package ivy
  :ensure t
  :init
  (setq ivy-use-virtual-buffers t
        ivy-height 15                             ; muestra 15 líneas de información (por defecto son 10)
        ivy-count-format ""                       ; no mostrar el contador
        ivy-initial-inputs-alist nil)
  :config
  (ivy-mode t))

(use-package ivy-rich
  :ensure t
  :config
  (ivy-rich-mode t))

(use-package nerd-icons-ivy-rich
  :ensure t
  :init
  (nerd-icons-ivy-rich-mode 1)
  (ivy-rich-mode 1))

;; Visualizar iconos
(setq nerd-icons-ivy-rich-icon t)

;; Visualiza los iconos en color
;; respetando `nerd-icons-color-icons'.
(setq nerd-icons-ivy-rich-color-icon t)

;; Tamaño del icono
(setq nerd-icons-ivy-rich-icon-size 1.0)

;; Soporte para project
(setq nerd-icons-ivy-rich-project t)

;; Definitions for ivy-rich transformers.
;; See `ivy-rich-display-transformers-list' for details."
nerd-icons-ivy-rich-display-transformers-list

;; Renderizado lento
;; Si experimentas un enlentecimiento mientras se renderizan varios iconos de manera simultánea,
;; puedes probar a establecer la siguiente variable.
(setq inhibit-compacting-font-caches t)

;; Utilizar ivy para ver alternativas de xref
(use-package ivy-xref
  :defer t
  :after ivy)

;; Configuración para projectile
(use-package projectile
  :ensure t
  :delight " Prjtl"
  :bind-keymap (("C-c p" . projectile-command-map))
  :init
  (setq projectile-project-search-path '("~/proyectos/"))
  (setq projectile-completion-system 'ivy)
  (projectile-mode t))
;; autocompletado para `projectile'
(use-package flycheck-projectile
  :ensure t
  :defer t)

;; gestión de plantillas yasnippet
(use-package yasnippet
  :ensure t
  :defer t)

(use-package ivy-yasnippet
  :ensure t
  :defer t)

;; colección de plantillas para yasnippet
(use-package yasnippet-snippets
  :ensure t
  :defer t)

;; activar yasnippet donde es necesario
(add-hook 'org-mode-hook 'yas-minor-mode)
(add-hook 'prog-mode-hook 'yas-minor-mode)
(add-hook 'markdown-mode-hook 'yas-minor-mode)

;; asociar tecla con mostrar lista de snippets
(global-set-key (kbd "C-c y") 'ivy-yasnippet)

(use-package eldoc-box
  :ensure t
  :defer t
  :delight " EDBox"
  :init (setq eldoc-box-hover-at-point-mode t))

(add-hook 'eglot-managed-mode-hook #'eldoc-box-hover-mode t)

(use-package flymake
  :ensure t
  :defer t
  :hook
  (prog-mode . flymake-mode))

(use-package flymake-flycheck
  :ensure t
  :defer t
  :hook
  (flymake-mode-hook . flymake-flycheck-auto))

(use-package which-key
  :ensure t
  :init
  (which-key-setup-minibuffer)
  (which-key-mode))

(use-package perspective
  :ensure t
  :init
  (persp-mode t))

(use-package w3m
  :ensure t
  :defer t)

;; Preparando la base de datos bbdb
(use-package bbdb
  :init
  (setq bbdb-file "~/agenda/especiales/bbdb.gpg"
        bbdb-phone-style nil)
  :defer t
  :config
  (bbdb-initialize))

(use-package org-contrib
  :defer t
  :after org
  :config
  (require 'ox-extra)
  (ox-extra-activate '(ignore-headlines)))

(use-package languagetool
  :ensure t
  :defer t
  :commands (languagetool-check
             languagetool-clear-suggestions
             languagetool-correct-at-point
             languagetool-correct-buffer
             languagetool-set-language
             languagetool-server-mode
             languagetool-server-start
             languagetool-server-stop))

(setq languagetool-java-arguments '("-Dfile.encoding=UTF-8"))
(setq languagetool-console-command "~/opt/LanguageTool-6.4/languagetool-commandline.jar")
(setq languagetool-server-command "~/opt/LanguageTool-6.4/languagetool-server.jar")

(global-set-key (kbd "C-c n c") 'languagetool-check)
(global-set-key (kbd "C-c n i") 'languagetool-server-start)
(global-set-key (kbd "C-c n q") 'languagetool-server-stop)
(global-set-key (kbd "C-c n s") 'languagetool-server-mode)

(use-package elfeed
  :defer t
  :ensure t)

;;(load-file "~/.emacs.d/notxor-blog/notxor-blog.el")
;; (use-package notxor-blog
;;   :load-path "~/.emacs.d/notxor-blog/"
;;   :defer t
;;   :ensure t)

;; Guardar en un registro el fichero de bitácora para abrirlo en cualquier momento
(set-register ?a '(file . "~/agenda/bloc-notas.org"))

;; Tomar notas
(use-package deft
  :ensure t
  :bind ("<f5>" . deft)
  :commands (deft deft-refresh)
  :config (setq deft-directory "~/Notas"
                deft-extensions '("org" "md" "txt")
                deft-use-filename-as-title t))

;; Cambio de directorio Deft
(defun deft-cambio-dir (dir)
  "Cambiar de directorio Deft al `DIR' de forma interactiva."
  (interactive "DNuevo directorio Deft: ")

  (message (format "Se cambia def-directory a: %s" dir))
  (setq deft-directory (expand-file-name dir))
  (deft-refresh))

(defun deft-recursivo ()
  "Establece que `deft' busque también en subdirectorios."
  (interactive)
  (setq deft-recursive t)
  (deft-refresh))

(defun deft-no-recursivo ()
  "Hace que `deft' no busque en subdirectorios."
  (interactive)
  (setq deft-recursive nil)
  (deft-refresh))

(global-set-key (kbd "C-<f5>") #'deft-cambio-dir)
(global-set-key (kbd "M-<f5>") #'deft-recursivo)
(global-set-key (kbd "S-<f5>") #'deft-no-recursivo)

(use-package zetteldeft
  :ensure t
  :defer t
  :after deft
  :config (zetteldeft-set-classic-keybindings))

(use-package emacsql
  :ensure t)
(use-package emacsql-sqlite
  :ensure t
  :after 'emacsql)

(use-package howm
  :ensure t
  :config
  (setq howm-home-directory "~/Notas/")
  (setq howm-directory "~/Notas/")
  (setq howm-keyword-file (expand-file-name ".howm-keys" howm-home-directory))
  (setq howm-history-file (expand-file-name ".howm-history" howm-home-directory))
  (setq howm-file-name-format "%Y/%m/%Y-%m-%d-%H%M%S.org"))

(use-package treemacs
  :ensure t
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                   (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay        0.5
          treemacs-directory-name-transformer      #'identity
          treemacs-display-in-side-window          t
          treemacs-eldoc-display                   'simple
          treemacs-file-event-delay                2000
          treemacs-file-extension-regex            treemacs-last-period-regex-value
          treemacs-file-follow-delay               0.2
          treemacs-file-name-transformer           #'identity
          treemacs-follow-after-init               t
          treemacs-expand-after-init               t
          treemacs-find-workspace-method           'find-for-file-or-pick-first
          treemacs-git-command-pipe                ""
          treemacs-goto-tag-strategy               'refetch-index
          treemacs-header-scroll-indicators        '(nil . "^^^^^^")
          treemacs-hide-dot-git-directory          t
          treemacs-indentation                     2
          treemacs-indentation-string              " "
          treemacs-is-never-other-window           nil
          treemacs-max-git-entries                 5000
          treemacs-missing-project-action          'ask
          treemacs-move-files-by-mouse-dragging    t
          treemacs-move-forward-on-expand          nil
          treemacs-no-png-images                   nil
          treemacs-no-delete-other-windows         t
          treemacs-project-follow-cleanup          nil
          treemacs-persist-file                    (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                        'left
          treemacs-read-string-input               'from-child-frame
          treemacs-recenter-distance               0.1
          treemacs-recenter-after-file-follow      nil
          treemacs-recenter-after-tag-follow       nil
          treemacs-recenter-after-project-jump     'always
          treemacs-recenter-after-project-expand   'on-distance
          treemacs-litter-directories              '("/node_modules" "/.venv" "/.cask")
          treemacs-project-follow-into-home        nil
          treemacs-show-cursor                     nil
          treemacs-show-hidden-files               t
          treemacs-silent-filewatch                nil
          treemacs-silent-refresh                  nil
          treemacs-sorting                         'alphabetic-asc
          treemacs-select-when-already-in-treemacs 'move-back
          treemacs-space-between-root-nodes        t
          treemacs-tag-follow-cleanup              t
          treemacs-tag-follow-delay                1.5
          treemacs-text-scale                      nil
          treemacs-user-mode-line-format           nil
          treemacs-user-header-line-format         nil
          treemacs-wide-toggle-width               70
          treemacs-width                           35
          treemacs-width-increment                 1
          treemacs-width-is-initially-locked       t
          treemacs-workspace-switch-cleanup        nil)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode 'always)
    (when treemacs-python-executable
      (treemacs-git-commit-diff-mode t))

    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple)))

    (treemacs-hide-gitignored-files-mode nil))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t d"   . treemacs-select-directory)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

(use-package treemacs-projectile
  :after (treemacs projectile)
  :ensure t)

(use-package treemacs-magit
  :after (treemacs magit)
  :ensure t)

(use-package treemacs-perspective ;;treemacs-perspective if you use perspective.el vs. persp-mode
  :after (treemacs perspective) ;;or perspective vs. persp-mode
  :ensure t
  :config (treemacs-set-scope-type 'Perspectives))

(use-package treemacs-tab-bar ;;treemacs-tab-bar if you use tab-bar-mode
  :after (treemacs)
  :ensure t
  :config (treemacs-set-scope-type 'Tabs))

(use-package treemacs-nerd-icons
  :config
  (treemacs-load-theme "nerd-icons"))

;; Activar autocompletado general
(use-package company
  :ensure t
  :init
  (setq company-tooltip-align-annotations t))

(add-hook 'after-init-hook 'global-company-mode)

;; Evita el efecto desalineado en los marcos con diferente
;; tamaño de letra en el modo consola
(use-package company-posframe
  :after company
  :config
  (company-posframe-mode t)
  :delight " C-PF")

(use-package company-box
  :ensure t
  :defer t
  :after company
  :delight " CBox"
  :hook (company-mode-hook . company-box-mode))

(use-package company-web
  :ensure t
  :defer t
  :after company)

;;configuración para contabilidad
(use-package ledger-mode
  :ensure t
  :defer t
  :config
  (setq ledger-reconcile-default-commodity "€"
        ledger-default-date-format "%Y-%m-%d"))
(autoload 'ledger-mode "ledger-mode" "Modo mayor para Ledger" t)
(add-to-list 'auto-mode-alist '("\\.ledger$" . ledger-mode))
(add-hook 'ledger-mode-hook
          (lambda ()
            (setq-local tab-always-indent 'complete)
            (setq-local completion-cycle-threshold t)
            (setq-local ledger-complete-in-steps t)
            (flymake-mode -1)
            (flycheck-mode t)))

(use-package company-ledger
   :ensure t
   :after company
   :defer t
   :init
   (with-eval-after-load 'company
     (add-to-list 'company-backends 'company-ledger)))

(use-package flycheck-ledger
  :ensure t
  :defer t
  :after flycheck)

(use-package org-auto-tangle
  :defer t)
  ;; :hook (org-mode . org-auto-tangle-mode)
  ;; :config
  ;; (setq org-auto-tangle-default t))

;; configuración para flycheck
(use-package flycheck
  :ensure t
  :defer t
  :hook
  ((tcl-mode . flycheck-mode)
   (tcl-mode . flycheck-tcl-setup)
   (ledger-mode . flycheck-mode)))
;; (add-hook 'prog-mode-hook 'flycheck-mode)

(use-package flycheck-pos-tip
  :ensure t
  :defer t
  :hook
  (flycheck-mode . flycheck-pos-tip-mode))

;;magit
(use-package magit
  :ensure t)
(use-package magit-stats
  :ensure t)
;; fossil
(use-package vc-fossil
  :defer t)

;; Configurar ‘iedit’
(use-package iedit
  :ensure t)

(use-package nerd-icons-dired
  :hook
  (dired-mode . nerd-icons-dired-mode))

(use-package nerd-icons-ibuffer
  :ensure t
  :hook (ibuffer-mode . nerd-icons-ibuffer-mode))

(use-package nerd-icons-completion
  :config
  (nerd-icons-completion-mode))

(use-package ibuffer-sidebar
  :defer t)

;; Proporciona terminal flexible
(use-package eat
  :ensure t
  :defer t
  :config
  (eat-eshell-mode t))

;; Paquete para asciidoc
(use-package adoc-mode
  :defer t)
(add-to-list 'auto-mode-alist '("\\.asciidoc\\'" . adoc-mode))

(use-package auctex
  :defer t)

;; Soporte de autocompletado para LaTeX
(use-package company-auctex
  :ensure t
  :after company)

;; Ajustes para AUCTeX
(setq TeX-view-program-selection
      '(((output-dvi has-no-display-manager)
         "dvi2tty")
        ((output-dvi style-pstricks)
         "dvips and gv")
        (output-dvi "xdvi")
        (output-pdf "xdg-open")
        (output-html "xdg-open")))
(add-hook 'LaTeX-mode-hook (lambda ()(TeX-electric-math t)))
(add-hook 'LaTeX-mode-hook 'turn-on-cdlatex)               ;Activar CDLaTeX
(add-hook 'LaTeX-mode-hook (lambda ()(company-mode 1)))    ;Activar Autocompletado
(company-auctex-init)
(add-hook 'LaTeX-mode-hook (lambda ()(TeX-fold-mode 1)))   ;Activar el plegado
(add-hook 'LaTeX-mode-hook 'latex-extra-mode)
(add-hook 'LaTeX-mode-hook 'display-line-numbers-mode)
(add-hook 'LaTeX-mode-hook 'eglot-ensure)

;; (use-package ox-tufte
;;   :defer t)

;; cargar el modo LaTeX si se habre un `.tex'
(add-to-list 'auto-mode-alist '("\\.tex\\'" . LaTeX-mode))

;; Managing Bibliographies
(use-package bibtex
  :custom
  (bibtex-dialect 'BibTeX)
  (bibtex-user-optional-fields
   '(("Etiquetas" "Etiquetas para describir la entrada" "")
     ("archivo" "Enlace a un documento." "" )))
  (bibtex-align-at-equal-sign t))

(use-package citeproc
  :defer t
  :ensure t)
(use-package citeproc-org
  :defer t
  :ensure t
  :after citeproc)

(use-package plantuml-mode
  :defer t)

(use-package pikchr-mode
  :defer t)

;; Configurar web-mode
(use-package web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.js[x]?\\'" . web-mode))

(use-package simple-httpd
  :ensure t
  :defer t
  :config (setq httpd-port 8080
                httpd-root "~/public_html"))

(use-package markdown-mode
  :ensure t
  :defer t
  :delight " M↓")
(use-package markdown-ts-mode
  :ensure t
  :defer t)
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

(use-package ag
  :ensure t
  :defer t)

(use-package gnuplot
  :ensure t
  :defer t)
(autoload 'gnuplot-mode "gnuplot" "Gnuplot major mode" t)
(autoload 'gnuplot-make-buffer "gnuplot" "open a buffer in gnuplot-mode" t)
(setq auto-mode-alist (append '(("\\.gp$" . gnuplot-mode)) auto-mode-alist))

(use-package geiser
  :ensure t
  :defer t)

(use-package geiser-guile
  :after geiser
  :defer t)

(use-package geiser-mit
  :after geiser
  :defer t)

(use-package geiser-chibi
  :after geiser
  :defer t)

(use-package geiser-chicken
  :after geiser
  :defer t)

(use-package flymake-guile
  :defer t)

(load (expand-file-name "~/quicklisp/slime-helper.el"))

(use-package slime
  :defer t
  :config
  (setq inferior-lisp-program "/usr/bin/sbcl"))

(use-package slime-company
  :defer t
  :after slime)

(use-package eglot
  :defer t)

(use-package eglot-java
  :defer t
  :after eglot)

(add-hook 'java-mode-hook 'eglot-ensure)
(add-hook 'java-mode-hook 'eglot-java-mode)
(add-hook 'eglot-managed-mode-hook #'eldoc-box-hover-mode t)
(with-eval-after-load 'eglot-java
  (define-key eglot-java-mode-map (kbd "C-c l n") #'eglot-java-file-new)
  (define-key eglot-java-mode-map (kbd "C-c l x") #'eglot-java-run-main)
  (define-key eglot-java-mode-map (kbd "C-c l t") #'eglot-java-run-test)
  (define-key eglot-java-mode-map (kbd "C-c l N") #'eglot-java-project-new)
  (define-key eglot-java-mode-map (kbd "C-c l T") #'eglot-java-project-build-task)
  (define-key eglot-java-mode-map (kbd "C-c l R") #'eglot-java-project-build-refresh))

(with-eval-after-load 'flymake-mode
  (define-key flymake-mode-map (kbd "M-n") #'flymake-goto-next-error))

(use-package flymake-gradle
  :defer t)

(use-package java-snippets
  :defer t)

(use-package dape
  :defer t)

(use-package clojure-mode
  :defer t)
(use-package cider
  :defer t)
(use-package flycheck-clojure
  :defer t)
(use-package clojure-snippets
  :ensure t)
(use-package ivy-clojuredocs
  :ensure t)
(use-package clj-refactor
  :defer t)
(use-package cljr-ivy
  :defer t)
(use-package clojure-ts-mode
  :defer t)
(add-hook 'clojure-mode-hook #'cider-mode)
(add-hook 'clojure-mode-hook #'eglot-ensure)
(add-hook 'clojure-mode-hook #'clojure-ts-mode)

(use-package clojure-essential-ref
  :defer t
  :bind (
         :map cider-mode-map
         ("C-h F" . clojure-essential-ref)
         :map cider-repl-mode-map
         ("C-h F" . clojure-essential-ref)))

(use-package lua-mode
  :defer t)
;; autocompletado para `lua'
(use-package company-lua
  :defer t)
(use-package flymake-lua
  :defer t)

(add-hook 'lua-mode-hook 'eglot-ensure)

(use-package flycheck-tcl
  :defer t)
;; Añadir extensión `.test' para `tcl-mode'
(add-to-list 'auto-mode-alist '("\\.test\\'" . tcl-mode))
(add-hook 'tcl-mode-hook
          (lambda ()
            (flymake-mode -1)
            (flycheck-mode t)))

(use-package modern-cpp-font-lock
  :ensure t
  :init
  (add-hook 'c++-mode-hook #'modern-c++-font-lock-mode))

;; Configuración del entorno elpy para Python
(use-package elpy
  :ensure t
  :defer t
  :config
  (setq python-shell-interpreter "python3")
  (setq elpy-rpc-python-command "python3")
  :init
  (advice-add 'python-mode :before 'elpy-enable))
(add-hook 'python-mode-hook 'eglot-ensure)

;; Configurar el sitio de erlang
(use-package edts
  :defer t)

(use-package julia-mode
  :defer t)
(use-package julia-repl
  :defer t
  :init
  (add-hook 'julia-mode-hook 'julia-repl-mode))
(use-package flycheck-julia
  :defer t)
(use-package julia-ts-mode
  :defer t
  :init
  (add-hook 'julia-mode-hook #'julia-ts-mode))

(use-package rust-mode
  :defer t
  :init
  (setq ruts-mode-treesitter-derive t))

;; (use-package tree-sitter
;;   :defer t
;;   :config
;;   (require 'tree-sitter-langs)
;;   (global-tree-sitter-mode)
;;   (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))

(use-package cargo-mode
  :defer t
  :hook
  (rust-mode . cargo-minor-mode)
  :config
  (setq compilation-scroll-output t))

(add-hook 'rust-mode-hook 'eglot-ensure)

(use-package ligature
  :ensure t
  :config
  ;; Enable the "www" ligature in every possible major mode
  (ligature-set-ligatures 't '("www"))
  ;; Enable traditional ligature support in eww-mode, if the
  ;; `variable-pitch' face supports it
  (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
  ;; Enable all Cascadia and Fira Code ligatures in programming modes
  (ligature-set-ligatures 'prog-mode
                        '(;; == === ==== => =| =>>=>=|=>==>> ==< =/=//=// =~
                          ;; =:= =!=
                          ("=" (rx (+ (or ">" "<" "|" "/" "~" ":" "!" "="))))
                          ;; ;; ;;;
                          (";" (rx (+ ";")))
                          ;; && &&&
                          ("&" (rx (+ "&")))
                          ;; !! !!! !. !: !!. != !== !~
                          ("!" (rx (+ (or "=" "!" "\." ":" "~"))))
                          ;; ?? ??? ?:  ?=  ?.
                          ("?" (rx (or ":" "=" "\." (+ "?"))))
                          ;; %% %%%
                          ("%" (rx (+ "%")))
                          ;; |> ||> |||> ||||> |] |} || ||| |-> ||-||
                          ;; |->>-||-<<-| |- |== ||=||
                          ;; |==>>==<<==<=>==//==/=!==:===>
                          ("|" (rx (+ (or ">" "<" "|" "/" ":" "!" "}" "\]"
                                          "-" "=" ))))
                          ;; \\ \\\ \/
                          ("\\" (rx (or "/" (+ "\\"))))
                          ;; ++ +++ ++++ +>
                          ("+" (rx (or ">" (+ "+"))))
                          ;; :: ::: :::: :> :< := :// ::=
                          (":" (rx (or ">" "<" "=" "//" ":=" (+ ":"))))
                          ;; // /// //// /\ /* /> /===:===!=//===>>==>==/
                          ("/" (rx (+ (or ">"  "<" "|" "/" "\\" "\*" ":" "!"
                                          "="))))
                          ;; .. ... .... .= .- .? ..= ..<
                          ("\." (rx (or "=" "-" "\?" "\.=" "\.<" (+ "\."))))
                          ;; -- --- ---- -~ -> ->> -| -|->-->>->--<<-|
                          ("-" (rx (+ (or ">" "<" "|" "~" "-"))))
                          ;; *> */ *)  ** *** ****
                          ("*" (rx (or ">" "/" ")" (+ "*"))))
                          ;; www wwww
                          ("w" (rx (+ "w")))
                          ;; <> <!-- <|> <: <~ <~> <~~ <+ <* <$ </  <+> <*>
                          ;; <$> </> <|  <||  <||| <|||| <- <-| <-<<-|-> <->>
                          ;; <<-> <= <=> <<==<<==>=|=>==/==//=!==:=>
                          ;; << <<< <<<<
                          ("<" (rx (+ (or "\+" "\*" "\$" "<" ">" ":" "~"  "!"
                                          "-"  "/" "|" "="))))
                          ;; >: >- >>- >--|-> >>-|-> >= >== >>== >=|=:=>>
                          ;; >> >>> >>>>
                          (">" (rx (+ (or ">" "<" "|" "/" ":" "=" "-"))))
                          ;; #: #= #! #( #? #[ #{ #_ #_( ## ### #####
                          ("#" (rx (or ":" "=" "!" "(" "\?" "\[" "{" "_(" "_"
                                       (+ "#"))))
                          ;; ~~ ~~~ ~=  ~-  ~@ ~> ~~>
                          ("~" (rx (or ">" "=" "-" "@" "~>" (+ "~"))))
                          ;; __ ___ ____ _|_ __|____|_
                          ("_" (rx (+ (or "_" "|"))))
                          ;; Fira code: 0xFF 0x12
                          ("0" (rx (and "x" (+ (in "A-F" "a-f" "0-9")))))
                          ;; Fira code:
                          "Fl"  "Tl"  "fi"  "fj"  "fl"  "ft"
                          ;; The few not covered by the regexps.
                          "{|"  "[|"  "]#"  "(*"  "}#"  "$>"  "^="))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  ;;(global-ligature-mode t)
  :bind ("<f7>" . ligature-mode))

(provide 'init)
;;; init.el ends here
