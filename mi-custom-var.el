(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.

'(browse-url-secondary-browser-function 'browse-url-w3)
;; Estilos de cabeceras
'(calendar-date-style 'iso)
'(calendar-day-header-array ["Do" "Lu" "Ma" "Mi" "Ju" "Vi" "Sa"])
'(calendar-day-name-array
  ["domingo" "lunes" "martes" "miércoles" "jueves" "viernes" "sábado"])
'(calendar-month-abbrev-array
  ["Ene" "Feb" "Mar" "Abr" "May" "Jun" "Jul" "Ago" "Sep" "Oct" "Nov" "Dic"])
'(calendar-month-name-array
  ["enero" "febrero" "marzo" "abril" "mayo" "junio" "julio" "agosto" "septiembre" "octubre" "noviembre" "diciembre"])
'(calendar-week-start-day 1) ; La semana comienza en lunes

'(org-agenda-diary-file "~/agenda/diario.org")
'(org-agenda-files
  '("/home/notxor/agenda/agenda.org" "/home/notxor/agenda/personal.org"))
'(org-agenda-include-diary t)
'(org-archive-location "~/agenda/especiales/archive.org::* Desde %s")

'(org-caldav-calendar-id "personal")
'(org-caldav-files '("~/agenda/personal.org"))
'(org-caldav-inbox "~/agenda/personal.org")
'(org-caldav-url
  "https://nube.nueva-actitud.org/remote.php/dav/calendars/Notxor")
'(org-icalendar-timezone "Europe/Madrid")
'(org-todo-keywords
  '((sequence "PENDIENTE(p)" "ESPERANDO(e)" "|" "HECHO(h)" "CANCELADO(c)")))

'(diary-entry-marker 'font-lock-variable-name-face)
'(diary-file "~/agenda/diario.org")
'(calendar-mark-diary-entries-flag t)

'(org-capture-templates
    ;; Es una lista de listas así que vamos por partes
    '(("t" "Tarea Pendiente")
      ("tt" "Tarea Simple    (t) trabajo" entry (file "~/agenda/agenda.org")
       "* PENDIENTE %? \t :trabajo:
   :PROPERTIES:
   :CREATE:      %U
   :END:
   :LOGBOOK:
   - State  \"PENDIENTE\"            from \"\"      %U
   :END:" :empty-lines 1)
        ("ta" "Tarea Simple    (a) asociación" entry (file+headline "~/agenda/agenda.org" "Asociación")
         "* PENDIENTE %? \t :pica:
   :PROPERTIES:
   :CREATE:      %U
   :END:
   :LOGBOOK:
   - State  \"PENDIENTE\"            from \"\"      %U
   :END:" :empty-lines 1)
        ("tp" "Tarea Simple    (p) personal" entry (file+headline "~/agenda/agenda.org" "Personal")
         "* PENDIENTE %? \t :personal:
   :PROPERTIES:
   :CREATE:      %U
   :END:
   :LOGBOOK:
   - State  \"PENDIENTE\"            from \"\"      %U
   :END:" :empty-lines 1)
 ;;; Capturar tareas que pasan a estar a la espera
        ("e" "Tarea a la espera")
        ("et" "Tarea a la espera    (t) trabajo" entry (file "~/agenda/agenda.org")
         "* ESPERANDO %? \t :trabajo:
   :PROPERTIES:
   :CREATE:      %U
   :END:
   :LOGBOOK:
   - State  \"ESPERANDO\"            from \"\"      %U
   :END:" :empty-lines 1)
        ("ea" "Tarea a la espera    (a) asociación" entry (file+headline "~/agenda/agenda.org" "Asociación")
         "* ESPERANDO %? \t :pica:
   :PROPERTIES:
   :CREATE:      %U
   :END:
   :LOGBOOK:
   - State  \"ESPERANDO\"            from \"\"      %U
   :END:" :empty-lines 1)
        ("ep" "Tarea Simple    (p) personal" entry (file+headline "~/agenda/agenda.org" "Personal")
         "* ESPERANDO %? \t :personal:
   :PROPERTIES:
   :CREATE:      %U
   :END:
   :LOGBOOK:
   - State  \"ESPERANDO\"            from \"\"      %U
   :END:" :empty-lines 1)
 ;;; Capturas que tienen «deadline» asociada
        ("l" "Tarea con fecha límite")
        ("lt" "Tarea    (t) trabajo" entry (file "~/agenda/agenda.org")
         "* PENDIENTE %? \t :trabajo:
   DEADLINE: %(substring (call-interactively 'org-deadline) 12)
   :PROPERTIES:
   :CREATE:      %U
   :END:
   :LOGBOOK:
   - State  \"PENDIENTE\"            from \"\"      %U
   :END:" :empty-lines 1)
        ("la" "Tarea    (a) asociación" entry (file+headline "~/agenda/agenda.org" "Asociación")
         "* PENDIENTE %? \t :pica:
   DEADLINE: %(substring (call-interactively 'org-deadline) 12)
   :PROPERTIES:
   :CREATE:      %U
   :END:
   :LOGBOOK:
   - State  \"PENDIENTE\"            from \"\"      %U
   :END:" :empty-lines 1)
        ("lp" "Tarea    (p) personal" entry (file+headline "~/agenda/agenda.org" "Personal")
         "* PENDIENTE %? \t :personal:
   DEADLINE: %(substring (call-interactively 'org-deadline) 12)
   :PROPERTIES:
   :CREATE:      %U
   :END:
   :LOGBOOK:
   - State  \"PENDIENTE\"            from \"\"      %U
   :END:" :empty-lines 1)
 ;;; Templates de captura para cosas que no son para la agenda
        ("n" "Capturas no para agenda")
        ("nc" "Anotar (c) contacto" entry (file+headline "~/agenda/especiales/personal.org.gpg" "Sin ordenar")
         "** %^{Nombre} %^{Apellidos}%?
   :PROPERTIES:
   :Nombre:     %\\1
   :Apellidos:  %\\2
   :Alias:      %^{Alias}
   :Grupo:      %^{Grupo}
   :F-nacim:    %^{F-nacim}u
   :Móvil:      %^{Móvil}
   :Teléfono:
   :Email:      %^{Email}
   :Web:
   :Dirección:  %^{Dirección}
   :Ciudad:     %^{Ciudad}
   :Provincia:  %^{Provincia}
   :Cód.Pos:    %^{Código Postal}
   :Compañía:
   :Notas:
   :END:" :empty-lines 1)
        ("nd" "Anotar (d) diario" entry (file+headline "~/agenda/bitacora.org" "Diario")
         "** %U
%?" :empty-lines 1)
        ("ni" "Anotar (i) idea" entry (file+headline "~/agenda/bitacora.org" "Ideas")
         "** Idea para %^{Tema}
   :PROPERTIES:
   :Tema:      %\\1
   :fecha:     %U
   :END:
%?" :empty-lines 1)
    ("np" "Anotar (p) presupuesto" entry (file+headline "~/agenda/cuentas.org" "Presupuestos")
     "** Presupuesto para %^{Cliente}
   :PROPERTIES:
   :Cliente:       %\\1
   :fecha:         %U
   :END:
%T

| Concepto                              | Precio (€) | Cantidad  | Total (€) |
| <30>                                  | <9>        | <9>       | <9>       |
|---------------------------------------+------------+-----------+-----------|
| %?                                    |            |           |           |
|                                       |            |           |           |
|---------------------------------------+------------+-----------+-----------|
| Total                                 |            |           |           |
#+TBLFM: $4=$2*$3;%.2f::@>$4=vsum(@3..@-1);%.2f
" :empty-lines 1)
;;; Lista de templates de captura para Frateco
        ("f" "Tareas para Frateco Esperanto")
        ("ff" "Tarea    (f) Frateco" entry (file+headline "~/agenda/agenda.org" "Esperanto")
         "* PENDIENTE %? \t :personal:
   DEADLINE: %(substring (call-interactively 'org-deadline) 12)
   :PROPERTIES:
   :CREATE:       %U
   :END:
   :LOGBOOK:
   - State  \"PENDIENTE\"            from \"\"      %U
   :END:" :empty-lines 1)
        ))

'(elfeed-feeds
  '("https://trasteandoconjess.es/feed"
    "https://write.privacytools.io/c3po/feed/"
    "https://56k.es/rss"
    "https://ciberpatrulla.com/blog/feed"
    "https://victorhckinthefreeworld.com/feed/"
    "https://jordila.librebits.info/feed/"
    "https://notxor.nueva-actitud.org/rss.xml"
    "https://pfctelepathy.wordpress.com/feed/"
    "https://ondahostil.wordpress.com/feed/"
    "https://priioajn.wordpress.com/feed/"
    "https://izaroblog.com/feed/"
    "https://elpinguinotolkiano.wordpress.com/feed/"
    "http://www.lapipaplena.org/feed/"
    "http://www.infocop.es/AreaRSS/"
    "http://www.copmadrid.org/wp/feed/"
    "https://lamiradadelreplicante.com/feed/"
    "https://mamalinuxera.wordpress.com/feed/"
    "https://zagueros.noblogs.org/feed/"
    "https://colaboratorio.net/feed/"
    "https://maxxcan.codeberg.page/rss.xml"
    "https://mierda.tv/feed/"
    "http://rufianenlared.com/feed/"
    "https://lwn.net/headlines/newrss"
    "https://luluvonflama.wordpress.com/feed/"
    "https://lamaldiciondelescritor.blogspot.com/feeds/posts/default"
    "https://www.atareao.es/feed/"
    "https://elbinario.net/feed/"
    "https://linuxenmovimiento.es/feed/"
    "https://www.muylinux.com/feed/"
    "http://asociacionpica.org/feed"
    "https://nueva-actitud.org/feed/"
    "https://fatimenia.wordpress.com/feed/"
    "https://clarosenelbosque.com/feed/"
    "http://worldbuildingschool.com/feed/"
    "https://ugeek.github.io/feed.xml"
    "http://lapiedradesisifo.com/feed/"
    "http://elblogdeiulius.es/feed/page:feed.xml"
    "http://planet.emacs-es.org/rss20.xml"
    "http://ruifigueiredo.me/rss.xml"
    "https://gnutas.juanmanuelmacias.com/rss.xml"
    "https://quijotelibre.com/feed/"
    "https://ciberpatrulla.com/feed/"
    "https://ekaitz.elenq.tech/feeds/all.atom.xml"
    "https://angelesbroullon.gitlab.io/entredragonesypinguinos/atom.xml"
    "https://ebzzry.io/sitemap.xml"
    "http://muzaiko.info/public/podkasto/podkasto.rss"
    "https://unesperante.wordpress.com/feed/"
    "http://feeds.feedburner.com/VarsoviaVentoPodkasto"
    "http://www.esperanto.es/hef/index.php?format=feed&type=rss"
    "https://teokajlibroj.wordpress.com/feed/"
    "http://www.liberafolio.org/feed/"
    "https://eo.globalvoices.org/feed"
    "http://revuoesperanto.org/rss.xml"
    "http://eo.mondediplo.com/?page=backend"
    "https://bertilow.com/bertiloblogo/?feed=rss2"
    "https://scivolemo.wordpress.com/feed/"
    "http://feeds.feedburner.com/CuadernoDeCulturaCientfica"
    "https://lacienciaysusdemonios.com/feed/"
    "https://www.linux.com/feeds/rss"
    "http://planetkde.org/rss20.xml"
    "http://planet.kde-espana.org/atom.xml"
    "https://jordila.librebits.info/feed"
    "https://casatiajulia.com/blog/feed"
    "http://feeds.feedburner.com/blogelhackernet.xml"
    "https://elblogdelazaro.gitlab.io/index.xml"
    "https://www.davidrevoy.com/feed/rss"
    "https://adrianperales.com/feed/"
    "https://maestrapaladin.es/rss/rss.xml"
    "https://pedrolr.es/blog/feed/"))

'(Info-default-directory-list
  '("/usr/share/info/" "/usr/local/share/info/" "~/opt/share/info/"))
'(edts-man-root "/home/notxor/.emacs.d/edts/doc/R7B")

'(org-babel-load-languages
  '((dot . t)
    (plantuml . t)
    (org . t)
    (ledger . t)
    (python . t)
    (emacs-lisp . t)
    (scheme . t)
    (latex . t)
    (gnuplot . t)
    (sqlite . t)
    (shell . t)
    (awk . t)
    (sed . t)
    (tcl . t)
    (julia . t)
    (lua . t)))

'(org-plantuml-jar-path "~/opt/bin/plantuml.jar")
'(persp-mode-prefix-key (kbd "C-x c"))
'(org-startup-with-inline-images t)
'(org-startup-with-latex-preview t)
'(org-hide-emphasis-markers t) ; Elimina los caracteres de marca de org-mode

'(abbrev-suggest t)
'(delete-selection-mode nil)
'(display-line-numbers-type 'visual)
'(face-font-family-alternatives
  '(("Monospace" "Deja Vu Mono" "Fira Code" "fixed")
    ("Monospace Serif" "Fire Code" "Consolas" "Deja Vu Sans" "FreeMono" "Nimbus Mono L" "courier" "fixed")
    ("courier" "CMU Typewriter Text" "fixed")
    ("Sans Serif" "helv" "helvetica" "arial" "fixed")
    ("helv" "helvetica" "arial" "fixed")))
'(ansi-color-faces-vector
  [default default default italic underline success warning error])
'(ansi-color-names-vector
  ["black" "red3" "ForestGreen" "yellow3" "blue" "magenta3" "DeepSkyBlue" "gray50"])
'(column-number-mode t)
'(custom-enabled-themes '(dracula))
'(httpd-host 'local)
'(indent-tabs-mode nil)
'(pdf-view-midnight-colors '("#232629" . "#f8f8f2"))
;; Cuestiones `estéticas'
'(inhibit-startup-screen t)             ;No muestra la pantalla de inicio por defecto de `Emacs'
'(scroll-bar-mode nil)                  ;Elimina las barras de scroll
'(tool-bar-mode nil)                    ;Elimina la barra de herramientas
'(menu-bar-mode nil)                    ;Elimina el menú (accesible desde `<f10>')
'(global-prettify-symbols-mode t)       ;Activa la sustitución de símbolos de manera global

'(electric-pair-mode t)                 ;Activar el modo de autocierre de paréntesis
'(truncate-lines t)                     ;Las líneas en una línea sin saltos
'(show-paren-mode t)                    ;Remarca la pareja de un paréntesis (corchete, llave)
'(which-key-mode t)                     ;Activa la sugerencia de teclas
'(word-wrap t)
'(org-fontify-emphasized-text t))       ;Activar la visualización de estilos de fuente
;; Establecer el tamaño de inicio de la ventana
(add-to-list 'default-frame-alist '(width . 200)) ;Son valores en caracteres, dependen del tamaño de fuente
(add-to-list 'default-frame-alist '(height . 58))
(save-place-mode t)         ; Guarda la posición del cursor al cerrar un archivo
(global-auto-revert-mode t) ; Recarga el contenido del buffer si ha cambiado el archivo en disco.
(global-eldoc-mode t)
(setq global-auto-revert-non-file-buffers t) ; recarga buffers como el de dired si ha cambiado

;; Centralizando los ficheros temporales en el directorio temporal
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; Ajustes de fuentes y tipos de letra
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 ;;'(default ((t (:family "DejaVu Sans Mono" :foundry "PfEd" :slant normal :weight normal :height 98 :width normal))))
 ;;'(default ((t (:family "Fira Code" :foundry "CTDB" :slant normal :weight normal :height 98 :width normal))))
 ;;'(dired-sidebar-face ((t (:slant normal :weight normal :height 88 :width normal :foundry "CTDB" :family "Fira Code"))) t)
 '(org-tree-slide-heading-level-1 ((t (:inherit outline-1 :weight bold :height 3.5))))
 '(org-tree-slide-heading-level-2 ((t (:inherit outline-2 :weight bold :height 2.5))))
 '(org-tree-slide-heading-level-3 ((t (:inherit outline-3 :weight bold :height 1.5))))
 '(persp-selected-face ((t (:foreground "yellow" :weight normal)))))
